from flask import Flask
from flask import request, jsonify
from datetime import datetime
from dateutil import relativedelta
import json
app = Flask(__name__)
#For later from flask import render_template for html

log_file = None

# For a haandtere nedbetalingsplan av laan data
@app.route("/nedbetalingsplanLaan", methods=['POST'])
def post_logfile():
    
    if request.method == 'POST':
        log_file = request.form
        if log_file is not None:
            request_data = request.get_json()
            laanebelop = None
            nominellRente = None
            terminGebyr = None
            utlopsDato = None
            saldoDato = None
            datoForsteInnbetaling = None
            ukjentVerdi = None
            if 'nominellRente' in request_data:
                nominellRente = request_data['nominellRente'] 

            if 'laanebelop' in request_data:
                laanebelop = request_data['laanebelop']

            if 'terminGebyr' in request_data:
                terminGebyr = request_data['terminGebyr']    
            
            if 'utlopsDato' in request_data:
                utlopsDato = request_data['utlopsDato']
            
            if 'saldoDato' in request_data:
                saldoDato = request_data['saldoDato']
            
            if 'datoForsteInnbetaling' in request_data:
                datoForsteInnbetaling = request_data['datoForsteInnbetaling']
            
            if 'ukjentVerdi' in request_data:
                ukjentVerdi = request_data['ukjentVerdi']
            if(laanebelop != None and terminGebyr != None and utlopsDato != None and saldoDato != None and datoForsteInnbetaling != None and ukjentVerdi != None and nominellRente != None):
                return calculateMorgage(laanebelop, terminGebyr, utlopsDato,saldoDato,datoForsteInnbetaling,ukjentVerdi, nominellRente)
            else:
                return "Not enough data provided", 400
            
        else:
            return "No data provided", 400

# For handtere bsudata
@app.route("/bsukalkulator", methods=['POST'])
def bsu_inn():
    if request.method == 'POST':
        log_file = request.form

        if log_file is not None:
            request_data = request.json
            start_saldo = None
            antall_aar = None
            rente = None
            anualinnskudd = None

            if 'startSaldo' in request_data:
                start_saldo = request_data['startSaldo']
            if 'antallaar' in request_data:
                antall_aar = request_data['antallaar']
            if 'rente' in request_data:
                rente = request_data['rente']
            if 'anualinnskudd' in request_data:
                anualinnskudd = request_data['anualinnskudd']
            if start_saldo != None and antall_aar != None and rente != None and anualinnskudd != None:
                return calculateBSU(start_saldo, antall_aar, rente, anualinnskudd)
            else: 
                return "Not enough data provided" , 400
        else:
            return "No data provided", 400

def calculateMorgage(laanebelop, terminGebyr, utlopsDato, saldoDato, datoForsteInnbetaling, ukjentVerdi, nominellRente):

    #Omjor string til datetime
    datoUtlop_date = datetime.strptime(utlopsDato, '%Y-%m-%d')
    datoSaldo_date = datetime.strptime(saldoDato, '%Y-%m-%d')
    datoForsteInnbetaling_dato = datetime.strptime(datoForsteInnbetaling, '%Y-%m-%d')

    laanetidDelta = relativedelta.relativedelta(datoUtlop_date,datoForsteInnbetaling_dato) #Regner ut forskjellen i tid
    laanetidAr = laanetidDelta.years # finner ut hvor mange aar det er
    laanetidMonths = laanetidDelta.months # Finner ut hvor mange maaneder det er i tillegg
    samletLaanetidMonths = (laanetidAr * 12) + laanetidMonths #Summerer maaneder og maaned per aar som da er antall maaneder laanet skal være paa
    print(samletLaanetidMonths)
    print(datoUtlop_date)
    print(datoForsteInnbetaling_dato)
    innbetalinger_array = []
    input_laanebelop = laanebelop

    R = 1 + (nominellRente)/(100 * 12)
    X = input_laanebelop * (R**samletLaanetidMonths) * (1-R)/(1-R**samletLaanetidMonths)
    
    for i in range(0,samletLaanetidMonths):
        #Haandterer dato til streng
        dato  = datoForsteInnbetaling_dato + relativedelta.relativedelta(months=+i)
        dato_string = dato.strftime('%Y-%m-%d')

        renter = float(laanebelop) *(R-1)
        laanebelop = laanebelop - (X - renter)
        
        innbetaling = X - renter# laanebelop - restgjeld = innbetaling

        total = innbetaling + terminGebyr + renter
        innbetalinger_array.append({'dato' : dato_string, 'gebyr' : terminGebyr, 'renter':renter, 'restgjeld' : laanebelop, 'innbetaling' : innbetaling, 'total' : total})
    
    
    innbetalinger = jsonify(innbetalinger = innbetalinger_array)
   
    return innbetalinger
# Regner ut hvor mye man sitter igjen med etter å ha spart i antall år med et likt årlig innskudd
def calculateBSU(start_saldo, antall_aar, rente, anualinnskudd):
    nySaldo = start_saldo
    saldo_list = []
    for i in range(1, antall_aar + 1):
        nySaldo = nySaldo*(1+(rente/100)) + anualinnskudd
        
        saldo_list.append({'aar': i, 'saldo' : nySaldo})
            
       
    saldoer = jsonify(saldoer = saldo_list)
    print(saldoer)
    return saldoer

if __name__ == "__main__":
    app.run(port=9002)


##
#{
              #  "restgjeld": 100000.0,
               # "dato": "2020-01-01",
                #"innbetaling": 0.0,
               # "gebyr": 0.0,
               # "renter": 0.0,
                #"total": 0.0
            #},
##